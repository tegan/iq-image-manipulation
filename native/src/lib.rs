#[macro_use]
extern crate neon;
use neon::vm::{Call, JsResult, Lock};
use neon::js::binary::JsBuffer;
use neon::js::{JsBoolean, JsNumber, JsObject, Object};

extern crate rayon;
use rayon::prelude::*;

extern crate palette;
use palette::{FromColor, Hsl, Hue, Rgb, RgbHue, Saturate, Shade};

/// This function is made available to JavaScript as:
/// `function applyFilters(imageData, filterData) {...}`
///
/// `imageData` is an `ImageData` object resulting from calling `getImageData`
/// from an instance of `CanvasRenderingContext2D`.
///
/// `filterData` is a plain object describing the filter values to apply, e.g.,
/// ```
/// {
///     sepia: true|false,    // default: false
///     bw: true|false,       // default: false
///     brightness: -1.0-1.0, // default: 0
///     contrast: -1.0-1.0,   // default: 0
///     hue:: -1.0-1.0,       // default: 0
///     saturation: -1.0-1.0, // default: 0
///     red: -1.0-1.0,        // default: 0
///     green: -1.0-1.0,      // default: 0
///     blue: -1.0-1.0,       // default: 0
/// }
/// ```
fn apply_filters(call: Call) -> JsResult<JsObject> {
    let scope = call.scope; // the js function call scope
    let args = call.arguments; // the js function call arguments

    // Get buffer from image data
    let image_data = args.require(scope, 0)?.check::<JsObject>()?; // the ImageData object from ctx.getImageData
    let mut buffer = image_data.get(scope, "data")?.check::<JsBuffer>()?; // image buffer

    // Get filter values
    let filters = args.require(scope, 1)?.check::<JsObject>()?; // the object specifying filter values
    let sepia = filters.get(scope, "sepia")?.check::<JsBoolean>()?.value();
    let bw = filters.get(scope, "bw")?.check::<JsBoolean>()?.value();
    let brightness = filters
        .get(scope, "brightness")?
        .check::<JsNumber>()?
        .value() as f32;
    let contrast = filters.get(scope, "contrast")?.check::<JsNumber>()?.value() as f32;
    let hue = filters.get(scope, "hue")?.check::<JsNumber>()?.value() as f32;
    let saturation = filters
        .get(scope, "saturation")?
        .check::<JsNumber>()?
        .value() as f32;
    let red_offset = filters.get(scope, "red")?.check::<JsNumber>()?.value() as f32;
    let green_offset = filters.get(scope, "green")?.check::<JsNumber>()?.value() as f32;
    let blue_offset = filters.get(scope, "blue")?.check::<JsNumber>()?.value() as f32;

    buffer.grab(|mut buff| {
        buff.as_mut_slice().par_chunks_mut(4).for_each(|subpixels| {
            let mut color: Rgb<f32> = Rgb::new_u8(subpixels[0], subpixels[1], subpixels[2]);

            if sepia {
                color = apply_sepia(color);
            }

            if bw {
                color = apply_bw(color);
            }

            if brightness != 0.0 {
                color = color.lighten(brightness);
            }

            if contrast != 0.0 {
                color = apply_contrast(color, contrast);
            }

            if hue != 0.0 {
                color = apply_hue(color, hue);
            }

            if saturation != 0.0 {
                color = apply_saturation(color, saturation);
            }

            if red_offset != 0.0 {
                color.red = color.red + red_offset;
            }

            if green_offset != 0.0 {
                color.green = color.green + green_offset;
            }

            if blue_offset != 0.0 {
                color.blue = color.blue + blue_offset;
            }

            // to_pixel clamps values 0.0 - 1.0 and returns them as a tuple
            let (r, g, b) = color.to_pixel::<(f32, f32, f32)>();

            // convert f32 to u8 and sets new values back to buffer
            subpixels[0] = (r * 255.0) as u8;
            subpixels[1] = (g * 255.0) as u8;
            subpixels[2] = (b * 255.0) as u8;
        })
    });

    Ok(image_data)
}

register_module!(m, { m.export("applyFilters", apply_filters) });

/////////////////////////////
// Filter helper functions
/////////////////////////////

fn apply_sepia(mut color: Rgb) -> Rgb {
    let avg = (color.red + color.green + color.blue) / 3.0;
    color.red = avg * 0.960784;
    color.green = avg * 0.882353;
    color.blue = avg * 0.784314;
    color
}

fn apply_bw(mut color: Rgb) -> Rgb {
    let avg = (color.red + color.green + color.blue) / 3.0;
    color.red = avg;
    color.green = avg;
    color.blue = avg;
    color
}

fn apply_contrast(mut color: Rgb, contrast: f32) -> Rgb {
    let multiplier = contrast + 1.0;
    color.red = multiplier * color.red;
    color.green = multiplier * color.green;
    color.blue = multiplier * color.blue;
    color
}

fn apply_hue(color: Rgb, hue: f32) -> Rgb {
    let mut hsl = Hsl::from_rgb(color);
    let hue_rotation = RgbHue::from(hue * 180.0); // in degrees
    hsl = hsl.shift_hue(hue_rotation);
    Rgb::from_hsl(hsl)
}

fn apply_saturation(color: Rgb, saturation: f32) -> Rgb {
    let mut hsl = Hsl::from_rgb(color);
    hsl = hsl.saturate(saturation);
    Rgb::from_hsl(hsl)
}
