var addon = require("../native");

var FILTER_DEFAULTS = {
  bw: false,
  sepia: false,
  brightness: 0,
  contrast: 0,
  hue: 0,
  saturation: 0,
  red: 0,
  green: 0,
  blue: 0
};

function applyFilters(imageData, filters) {
  return addon.applyFilters(imageData, filters);
}

function getFilters(color) {
  color = Object.assign({}, FILTER_DEFAULTS, color);
  return {
    bw: false,
    sepia: false,
    brightness: color.brightness / 100,
    contrast: color.contrast / 100,
    hue: color.hue / 100,
    saturation: color.saturation / 100,
    red: color.red / 255,
    green: color.green / 255,
    blue: color.blue / 255
  };
}

module.exports = {
  applyFilters,
  getFilters
};
